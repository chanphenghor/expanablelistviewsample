package com.kosign.expandablelistviewsample.item;

import java.util.List;

/**
 * author        : HOR CHANPHENG
 * date        : 3/11/2019
 * description    :
 */
public class ItemParent {
    private String date;
    private int childCount;
    private List<ItemChild> childList;

    public ItemParent() {
    }

    public ItemParent(String date, int childCount, List<ItemChild> childList) {
        this.date = date;
        this.childCount = childCount;
        this.childList = childList;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getChildCount() {
        return childCount;
    }

    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    public List<ItemChild> getChildList() {
        return childList;
    }

    public void setChildList(List<ItemChild> childList) {
        this.childList = childList;
    }
}
