package com.kosign.expandablelistviewsample.item;
/**
 * author        : HOR CHANPHENG
 * date        : 3/11/2019
 * description    :
 */
public class ItemChild {

    private String itemChildName;
    private String itemChildPrice;
    private boolean isSelected;


    public ItemChild() {
    }

    public ItemChild(String itemChildName, String itemChildPrice, boolean isSelected) {
        this.itemChildName = itemChildName;
        this.itemChildPrice = itemChildPrice;
        this.isSelected = isSelected;
    }

    public String getItemChildName() {
        return itemChildName;
    }

    public void setItemChildName(String itemChildName) {
        this.itemChildName = itemChildName;
    }

    public String getItemChildPrice() {
        return itemChildPrice;
    }

    public void setItemChildPrice(String itemChildPrice) {
        this.itemChildPrice = itemChildPrice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
