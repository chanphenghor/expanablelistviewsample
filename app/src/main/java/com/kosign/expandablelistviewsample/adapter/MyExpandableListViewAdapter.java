package com.kosign.expandablelistviewsample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kosign.expandablelistviewsample.R;
import com.kosign.expandablelistviewsample.item.ItemChild;
import com.kosign.expandablelistviewsample.item.ItemParent;

import java.util.List;

public class MyExpandableListViewAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<ItemParent> itemParentList;

    public MyExpandableListViewAdapter(Context mContext, List<ItemParent> itemParentList) {
        this.mContext = mContext;
        this.itemParentList = itemParentList;
    }

    @Override
    public int getGroupCount() {
        return itemParentList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return itemParentList.get(groupPosition).getChildList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return itemParentList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return itemParentList.get(groupPosition).getChildList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        convertView = layoutInflater.inflate(R.layout.item_parent,null);

        ItemParent itemParent = itemParentList.get(groupPosition);
        ItemParent itemParent1 = (ItemParent) getGroup(groupPosition); //The same

        TextView tv_date = convertView.findViewById(R.id.tv_date);
        TextView tv_child_count = convertView.findViewById(R.id.tv_item_count);
        tv_date.setText(itemParent.getDate());
        tv_child_count.setText(itemParent.getChildCount()+"");

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        convertView = layoutInflater.inflate(R.layout.item_child,null);

        ItemChild itemChild = (ItemChild) getChild(groupPosition,childPosition);

        TextView tv_name = convertView.findViewById(R.id.tv_name);
        TextView tv_price = convertView.findViewById(R.id.tv_price);
        ImageView iv_coca_can= convertView.findViewById(R.id.iv_coca_can);

        tv_name.setText(itemChild.getItemChildName());
        tv_price.setText(itemChild.getItemChildPrice());

        if(itemChild.isSelected()){
            iv_coca_can.setImageResource(R.drawable.ic_coca_can);
        }else {
            iv_coca_can.setImageResource(R.drawable.ic_empty_can);

        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
