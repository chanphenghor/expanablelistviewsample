package com.kosign.expandablelistviewsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;

import com.kosign.expandablelistviewsample.adapter.MyExpandableListViewAdapter;
import com.kosign.expandablelistviewsample.item.ItemChild;
import com.kosign.expandablelistviewsample.item.ItemParent;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ExpandableListView expandableListView;
    MyExpandableListViewAdapter myExpandableListViewAdapter;
    List<ItemParent> itemParentList;
    List<ItemChild> itemChildList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        expandableListView = findViewById(R.id.my_expandable_listview);

        // Array List init
        itemChildList = new ArrayList<>();
        itemParentList = new ArrayList<>();

        myExpandableListViewAdapter = new MyExpandableListViewAdapter(this,itemParentList);
        expandableListView.setAdapter(myExpandableListViewAdapter);

        //Disable toggle Parent click=============================
        expandableListView.setOnGroupClickListener (new ExpandableListView.OnGroupClickListener(){

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;
            }
        });
        //=========================================================

        insertStaticData();
    }

    private void insertStaticData() {

        for(int i = 0; i<5;i++){
            ItemChild itemChild = new ItemChild("Coca"+i,(29+i)+"$", i == 2);
            itemChildList.add(itemChild);
        }

        ItemParent itemParent = new ItemParent();
        itemParent.setDate("Today");
        itemParent.setChildList(itemChildList);
        itemParent.setChildCount(itemChildList.size());

        ItemParent itemParent1 = new ItemParent("Yesterday",itemChildList.size(),itemChildList);
        ItemParent itemParent2 = new ItemParent("Saturday",itemChildList.size(),itemChildList);
        ItemParent itemParent3 = new ItemParent("Friday",itemChildList.size(),itemChildList);
        itemParentList.add(itemParent);
        itemParentList.add(itemParent1);
        itemParentList.add(itemParent2);
        itemParentList.add(itemParent3);

        //Notify after added data to itemParentList
        myExpandableListViewAdapter.notifyDataSetChanged();

        //Automatic expand all group
        for(int i = 0; i< myExpandableListViewAdapter.getGroupCount(); i++){
            expandableListView.expandGroup(i);
        }
    }
}
